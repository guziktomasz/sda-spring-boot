package pl.sda.spring_boot_intro.service;


import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DateTimeServiceImpl implements DateTimeService {

    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
