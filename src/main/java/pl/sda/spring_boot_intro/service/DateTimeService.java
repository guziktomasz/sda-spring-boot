package pl.sda.spring_boot_intro.service;

import java.time.LocalDateTime;

@FunctionalInterface
public interface DateTimeService {

    LocalDateTime now();
}

