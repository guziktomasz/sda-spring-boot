package pl.sda.spring_boot_intro.service;

@FunctionalInterface
public interface DisplayService {

    void display(String text);
}
