package pl.sda.spring_boot_intro.domain;

import java.time.LocalDateTime;

public class Reminder {

    private final String text;

    private final LocalDateTime dateTime;

    public Reminder(String text, LocalDateTime dateTime) {
        this.text = text;
        this.dateTime = dateTime;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "Reminder{" + "text='" + text + '\'' + ", dateTime=" + dateTime + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Reminder reminder = (Reminder) o;

        if (!text.equals(reminder.text))
            return false;
        return dateTime.equals(reminder.dateTime);
    }

    @Override
    public int hashCode() {
        int result = text.hashCode();
        result = 31 * result + dateTime.hashCode();
        return result;
    }
}
