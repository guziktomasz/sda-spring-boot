package pl.sda.spring_boot_intro;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cglib.core.Local;
import org.springframework.context.annotation.Bean;
import pl.sda.spring_boot_intro.domain.Reminder;
import pl.sda.spring_boot_intro.service.ReminderExecutor;

import java.time.LocalDateTime;

@SpringBootApplication
public class SpringBootIntroApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootIntroApplication.class, args);
        System.out.println("test");
    }

    @Bean
    public CommandLineRunner cmdLineRunner(ReminderExecutor re) {
        return (args) -> {
            Reminder reminder = new Reminder("SPRING SDA!!!", LocalDateTime.now());
            re.addReminder(reminder);
            re.execute();
        };
    }
}
